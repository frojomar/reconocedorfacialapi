#!/bin/bash
host=localhost
port=29293
export FLASK_APP=./app.py
source venv/bin/activate
flask run -h $host -p $port
