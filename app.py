from flask import Flask, jsonify, request
import face_recognitionImpl as frImpl
import json
import readConfigurationsFile as conf
from flask_cors import CORS

configurations = {}
conf.loadConfigurations('settings.conf', configurations)
print(configurations)

URL_MONGO="http://"+str(configurations['HOST_API_MONGO'])+":"+str(configurations['PORT_API_MONGO'])

app = Flask(__name__)
CORS(app)

@app.route('/', methods=['POST'])
def reconocer_persona_api():

    print("New Petition: To Recognize a Person")

    json_data = json.loads(request.data.decode('utf8'))
    image64 = json_data['image']

    #print(image64)

    known_face_encodings = []
    known_face_ids= []
    frImpl.loadFaces(known_face_ids, known_face_encodings, URL_MONGO)

    persons,image=frImpl.recognizePersons(known_face_ids, known_face_encodings, image64, URL_MONGO)

    result={}
    result['persons']=persons
    result['image']=str(image.decode('ascii'))

    print("Result of recognize process:")
    print(result)

    return jsonify(result)

    #return jsonify("{image:"+str(result)+"}")

@app.route('/addPerson', methods=['POST'])
def nueva_persona_api():

    print("New Petition: Add a Codification for a Person")

    json_data = json.loads(request.data.decode('utf8'))
    image64 = json_data['image']
    id= json_data['person_id']

    #print(image64)
    print("Person ID:"+str(id))

    if str(id).strip().__eq__(""):
        return jsonify("{'status': 'errorId'}")

    else:
        face_encoding= frImpl.get_encoding(image64)

        if face_encoding.__len__() <= 0:
            return jsonify("{'status': 'errorImage'}")

        else:
            print(face_encoding)

            response=frImpl.addPersonCodification(id,face_encoding, URL_MONGO)

            if response.status_code==200:
                return jsonify("{'status': 'ok'}")
            else:
                return jsonify("{'status': 'mongoError'}")

if __name__=='__main__':
    app.run(host=str(configurations['HOST']), port=str(configurations['PORT']), debug=True, threaded=True)
