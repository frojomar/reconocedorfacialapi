import face_recognition
import cv2
import base64
import auxiliar_array_library as auxArrayLib
import requests
from requests import Response
import json

import io
import numpy as np
from PIL import Image


def addPersonCodification(id, codification, URL):
    print("Adding codification to person "+str(id))
    data={
        "person_id": id,
        "codification": codification.tolist()
    }
    print(data)
    print(json.dumps(data))
    try:
        response=requests.post(url=URL+"/persons/"+str(id)+"/codifications", data=json.dumps(data))
        if response.status_code == 200:
            print("Id of the new codification: "+str(response.json['insertedId']))
        else:
            print("Response is not good... (Response Code : "+str(response.status_code)+")")
    except Exception as e:
        print("EXCEPTION: We cannot connect with Mongo")
        print(e)
        return Response
    finally:
        return response


def get_encoding(image_base64):

    image_fit_encoding = []

    imgdata= stringToImage(image_base64)
    imgCV= toRGB(imgdata)
    cv2.imwrite('imageFit.png', imgCV)

    image_fit = face_recognition.load_image_file("imageFit.png")

    image_fit_encodings= face_recognition.face_encodings(image_fit)

    if image_fit_encodings.__len__() >0:
        image_fit_encoding= image_fit_encodings[0]

    return image_fit_encoding


def loadFaces(known_face_ids, known_face_encodings, URL):

    known_face_ids
    known_face_encodings
    print("Getting face encodings of known persons...")
    try:
        response = requests.get(url=URL + "/persons/codifications")
        if response.status_code == 200:
            data=response.json()
            print("Length of response data: "+str(len(data)))
            for value in data:
                person_id=(value['person_id'])["$oid"]
                codification=(value['codification'])
                known_face_ids.append(person_id)
                known_face_encodings.append(codification)
        else:
            print("Response is not good... (Response Code : "+str(response.status_code)+")")
    except Exception as e:
        print("EXCEPTION: We cannot connect with Mongo")
        print(e)


# Take in base64 string and return PIL image
def stringToImage(base64_string):
    imgdata = base64.b64decode(base64_string)
    return Image.open(io.BytesIO(imgdata))

# convert PIL Image to an RGB image( technically a numpy array ) that's compatible with opencv
def toRGB(image):
    return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)


def getNameById(id, URL):
    name="Not found"
    try:
        response = requests.get(url=URL + "/persons/" + str(id))
        if response.status_code == 200:
            data = response.json()
            if data:
                name=data['name']
                print("Person name: " + str(name))
            else:
                print("Not found person with ID '" + str(id) + "'")
        else:
            print("Response is not good... (Response Code : " + str(response.status_code) + ")")
    except Exception as e:
        print("EXCEPTION: We cannot connect with Mongo")
        print(e)
    finally:
        return name


def recognizePersons(known_face_ids, known_face_encodings, image64, URL):

    imgdata= stringToImage(image64)
    imgCV= toRGB(imgdata)
    cv2.imwrite('image.png', imgCV)

    #print(str(imgdata))

    img = cv2.imread('image.png')
    rgb_frame= face_recognition.load_image_file("image.png")

    # Find all the faces and face encodings in the frame of video
    face_locations = face_recognition.face_locations(rgb_frame)
    face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)

    #List of recognized persons in the photo, with information of recognizes process also.
    persons_recognized=[]

    # Loop through each face in this frame of video
    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
        distances = face_recognition.face_distance(known_face_encodings, face_encoding)
        name = "Unknown"
        distance=1
        id=0

        # If a match was found in known_face_encodings, just use the first one.
        if True in matches:
            match_index, distance = auxArrayLib.selectLowerValue(distances)
            id = known_face_ids[match_index]
            print("MATCHES:")
            print(matches)
            print("DISTANCES:")
            print(distances)
            name=getNameById(id, URL)

        face_result={ "person_id" : id,
                        "name": name,
                        "distance": distance,
                        "top" : top,
                        "right" : right,
                        "bottom" : bottom,
                        "left" : left}

        persons_recognized.append(face_result)

        # Draw a box around the face
        cv2.rectangle(img, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(img, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(img, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # result={}
    # result['persons']=persons_recognized
    #
    # try:
    #     retval, buffer = cv2.imencode('.jpg', img)
    #     img_encoded = base64.b64encode(buffer)
    #     result['image']=img_encoded
    # except:
    #     print(">> We CAN'T convert image to base64")
    #     result['image']=image64
    # finally:
    #     return result

    persons_recognized

    try:
        retval, buffer = cv2.imencode('.jpg', img)
        img_encoded = base64.b64encode(buffer)
        image=img_encoded
    except:
        print(">> We CAN'T convert image to base64")
        image=image64
    finally:
        return persons_recognized,image